#include <stdio.h>
#include <string.h>
#include "../lib/minunit/minunit.h"
#include "../../src/eventbus.h"

int tests_run = 0;

int mockCount = 0;

typedef struct mock_struct {
  int n;
} mock_struct;

typedef enum mock_enum_types {
  MockStruct
} mock_enum_types;

void mock_fnListener1(int type, va_list args) {
  int count = 0;
  while (count < type) {
    mock_struct *mockStruct = va_arg(args, struct mock_struct*);
    mockStruct->n++;
    count++;
  }
}

void mock_fnListener2(int type, va_list args) {
  int count;
  count = 0;
  while (type != End) {
    if(type == MockStruct) {
      mock_struct *mockStruct = va_arg(args, struct mock_struct*);
      //printf("n: %d\n", mockStruct->n);
      mockStruct->n++;
    }
    type = va_arg(args, enum mock_enum_types);
    count++;
  }
}

void mock_fnListener3(int type, va_list args) {
  int count = 0;
  while (count < type) {
    mock_struct *mockStruct = va_arg(args, struct mock_struct*);
    mockStruct->n++;
    count++;
  }
}

void mock_fnListener4(int type, va_list args) {
  mockCount++;
}

static char *test1_addEvent() {
  EventBus *eventBus = eventBus_makeEventBus();
  Event *event = eventBus_addEvent(eventBus, "SomeEvent");
  mu_assert("error, eventBus->eventsLength != 1", eventBus->eventsLength == 1);
  mu_assert("error, event->name != SomeEvent", (strcmp(event->name, "SomeEvent") == 0));
  eventBus_freeEventBus(eventBus);
  return 0;
}

static char *test2_getEvent() {
  EventBus *eventBus = eventBus_makeEventBus();
  eventBus_addEvent(eventBus, "SomeEvent");
  Event *event = eventBus_getEvent(eventBus, "SomeEvent");
  mu_assert("error, eventBus->eventsLength != 1", eventBus->eventsLength == 1);
  mu_assert("error, event == NULL", event != NULL);
  mu_assert("error, event->name != SomeEvent", (strcmp(event->name, "SomeEvent") == 0));
  eventBus_freeEventBus(eventBus);
  return 0;
}

static char *test3_addListenerInEvent() {
  EventBus *eventBus = eventBus_makeEventBus();
  eventBus_addEvent(eventBus, "SomeEvent");
  Event *event = eventBus_getEvent(eventBus, "SomeEvent");
  int listenerAddSuccess = eventBus_addListenerInEvent(event, mock_fnListener1, false);
  mu_assert("error, listenerAddSuccess != 0", listenerAddSuccess == 0);
  mu_assert("error, eventBus->eventsLength != 1", eventBus->eventsLength == 1);
  mu_assert("error, event == NULL", event != NULL);
  mu_assert("error, event->name != SomeEvent", (strcmp(event->name, "SomeEvent") == 0));
  mu_assert("error, event->listenersLength != 1", event->listenersLength == 1);
  mu_assert("error, event->listeners == NULL", event->listeners != NULL);
  mu_assert("error, event->listeners[0].fn != mock_fnListener1", event->listeners[0].fn == mock_fnListener1);
  mu_assert("error, event->listeners[0].isOnce != false", event->listeners[0].isOnce == false);
  eventBus_freeEventBus(eventBus);
  return 0;
}

static char *test4_on() {
  EventBus *eventBus = eventBus_makeEventBus();
  eventBus_on(eventBus, "SomeEvent", mock_fnListener1);
  Event *event = eventBus_getEvent(eventBus, "SomeEvent");
  mu_assert("error, eventBus->eventsLength != 1", eventBus->eventsLength == 1);
  mu_assert("error, event == NULL", event != NULL);
  mu_assert("error, event->name != SomeEvent", (strcmp(event->name, "SomeEvent") == 0));
  mu_assert("error, event->listenersLength != 1", event->listenersLength == 1);
  mu_assert("error, event->listeners == NULL", event->listeners != NULL);
  mu_assert("error, event->listeners[0].fn != mock_fnListener1", event->listeners[0].fn == mock_fnListener1);
  mu_assert("error, event->listeners[0].isOnce != false", event->listeners[0].isOnce == false);
  eventBus_freeEventBus(eventBus);
  return 0;
}

static char *test5_on_twoCalls() {
  EventBus *eventBus = eventBus_makeEventBus();
  eventBus_on(eventBus, "SomeEvent", mock_fnListener1);
  eventBus_on(eventBus, "SomeEvent", mock_fnListener1);
  Event *event = eventBus_getEvent(eventBus, "SomeEvent");
  mu_assert("error, eventBus->eventsLength != 1", eventBus->eventsLength == 1);
  mu_assert("error, event == NULL", event != NULL);
  mu_assert("error, event->name != SomeEvent", (strcmp(event->name, "SomeEvent") == 0));
  mu_assert("error, event->listenersLength != 2", event->listenersLength == 2);
  mu_assert("error, event->listeners == NULL", event->listeners != NULL);
  mu_assert("error, event->listeners[0].fn != mock_fnListener1", event->listeners[0].fn == mock_fnListener1);
  mu_assert("error, event->listeners[0].isOnce != false", event->listeners[0].isOnce == false);
  mu_assert("error, event->listeners[1].fn != mock_fnListener1", event->listeners[1].fn == mock_fnListener1);
  mu_assert("error, event->listeners[1].isOnce != false", event->listeners[1].isOnce == false);
  eventBus_freeEventBus(eventBus);
  return 0;
}

static char *test6_on_whenFnIsNULL() {
  EventBus *eventBus = eventBus_makeEventBus();
  eventBus_on(eventBus, "SomeEvent", NULL);
  Event *event = eventBus_getEvent(eventBus, "SomeEvent");
  mu_assert("error, eventBus->eventsLength != 1", eventBus->eventsLength == 1);
  mu_assert("error, event == NULL", event != NULL);
  mu_assert("error, event->name != SomeEvent", (strcmp(event->name, "SomeEvent") == 0));
  mu_assert("error, event->listenersLength != 0", event->listenersLength == 0);
  mu_assert("error, event->listeners != NULL", event->listeners == NULL);
  eventBus_freeEventBus(eventBus);
  return 0;
}

static char *test7_once() {
  EventBus *eventBus = eventBus_makeEventBus();
  eventBus_once(eventBus, "SomeEvent", mock_fnListener1);
  Event *event = eventBus_getEvent(eventBus, "SomeEvent");
  mu_assert("error, eventBus->eventsLength != 1", eventBus->eventsLength == 1);
  mu_assert("error, event == NULL", event != NULL);
  mu_assert("error, event->name != SomeEvent", (strcmp(event->name, "SomeEvent") == 0));
  mu_assert("error, event->listenersLength != 1", event->listenersLength == 1);
  mu_assert("error, event->listeners == NULL", event->listeners != NULL);
  mu_assert("error, event->listeners[0].fn != mock_fnListener1", event->listeners[0].fn == mock_fnListener1);
  mu_assert("error, event->listeners[0].isOnce != true", event->listeners[0].isOnce == true);
  eventBus_freeEventBus(eventBus);
  return 0;
}

static char *test8_once_whenFnIsNULL() {
  EventBus *eventBus = eventBus_makeEventBus();
  eventBus_once(eventBus, "SomeEvent", NULL);
  Event *event = eventBus_getEvent(eventBus, "SomeEvent");
  mu_assert("error, eventBus->eventsLength != 1", eventBus->eventsLength == 1);
  mu_assert("error, event == NULL", event != NULL);
  mu_assert("error, event->name != SomeEvent", (strcmp(event->name, "SomeEvent") == 0));
  mu_assert("error, event->listenersLength != 0", event->listenersLength == 0);
  mu_assert("error, event->listeners != NULL", event->listeners == NULL);
  eventBus_freeEventBus(eventBus);
  return 0;
}

static char *test9_getListenerIndexByFn() {
  EventBus *eventBus = eventBus_makeEventBus();
  eventBus_on(eventBus, "SomeEvent", mock_fnListener1);
  eventBus_once(eventBus, "SomeEvent", mock_fnListener2);
  Event *event = eventBus_getEvent(eventBus, "SomeEvent");
  int listenerIndex = eventBus_getListenerIndexByFn(event, mock_fnListener2);
  mu_assert("error, listenerIndex != 1", listenerIndex == 1);
  mu_assert("error, eventBus->eventsLength != 1", eventBus->eventsLength == 1);
  mu_assert("error, event == NULL", event != NULL);
  mu_assert("error, event->name != SomeEvent", (strcmp(event->name, "SomeEvent") == 0));
  mu_assert("error, event->listenersLength != 1", event->listenersLength == 2);
  mu_assert("error, event->listeners == NULL", event->listeners != NULL);
  mu_assert("error, event->listeners[0].fn != mock_fnListener1", event->listeners[0].fn == mock_fnListener1);
  mu_assert("error, event->listeners[0].isOnce != false", event->listeners[0].isOnce == false);
  mu_assert("error, event->listeners[1].fn != mock_fnListener1", event->listeners[1].fn == mock_fnListener2);
  mu_assert("error, event->listeners[1].isOnce != true", event->listeners[1].isOnce == true);
  eventBus_freeEventBus(eventBus);
  return 0;
}

static char *test10_getListenerIndexByFn_whenListenerNotExist() {
  EventBus *eventBus = eventBus_makeEventBus();
  eventBus_on(eventBus, "SomeEvent", mock_fnListener1);
  Event *event = eventBus_getEvent(eventBus, "SomeEvent");
  int listenerIndex = eventBus_getListenerIndexByFn(event, mock_fnListener2);
  mu_assert("error, listenerIndex != -1", listenerIndex == -1);
  mu_assert("error, eventBus->eventsLength != 1", eventBus->eventsLength == 1);
  mu_assert("error, event == NULL", event != NULL);
  mu_assert("error, event->name != SomeEvent", (strcmp(event->name, "SomeEvent") == 0));
  mu_assert("error, event->listenersLength != 1", event->listenersLength == 1);
  mu_assert("error, event->listeners == NULL", event->listeners != NULL);
  mu_assert("error, event->listeners[0].fn != mock_fnListener1", event->listeners[0].fn == mock_fnListener1);
  mu_assert("error, event->listeners[0].isOnce != false", event->listeners[0].isOnce == false);
  eventBus_freeEventBus(eventBus);
  return 0;
}

static char *test11_off() {
  EventBus *eventBus = eventBus_makeEventBus();
  eventBus_once(eventBus, "SomeEvent", mock_fnListener1);
  eventBus_off(eventBus, "SomeEvent", mock_fnListener1);
  Event *event = eventBus_getEvent(eventBus, "SomeEvent");
  mu_assert("error, eventBus->eventsLength != 1", eventBus->eventsLength == 1);
  mu_assert("error, event == NULL", event != NULL);
  mu_assert("error, event->name != SomeEvent", (strcmp(event->name, "SomeEvent") == 0));
  mu_assert("error, event->listenersLength != 0", event->listenersLength == 0);
  mu_assert("error, event->listeners != NULL", event->listeners == NULL);
  eventBus_freeEventBus(eventBus);
  return 0;
}

static char *test12_off_withTwoListenersRemoveTheFirst() {
  EventBus *eventBus = eventBus_makeEventBus();
  eventBus_on(eventBus, "SomeEvent", mock_fnListener1);
  eventBus_once(eventBus, "SomeEvent", mock_fnListener2);
  eventBus_off(eventBus, "SomeEvent", mock_fnListener1);
  Event *event = eventBus_getEvent(eventBus, "SomeEvent");
  mu_assert("error, eventBus->eventsLength != 1", eventBus->eventsLength == 1);
  mu_assert("error, event == NULL", event != NULL);
  mu_assert("error, event->name != SomeEvent", (strcmp(event->name, "SomeEvent") == 0));
  mu_assert("error, event->listenersLength != 0", event->listenersLength == 1);
  mu_assert("error, event->listeners != NULL", event->listeners != NULL);
  mu_assert("error, event->listeners[0].fn != mock_fnListener2", event->listeners[0].fn == mock_fnListener2);
  mu_assert("error, event->listeners[0].isOnce != true", event->listeners[0].isOnce == true);
  eventBus_freeEventBus(eventBus);
  return 0;
}

static char *test13_off_withTwoListenersRemoveTheSecond() {
  EventBus *eventBus = eventBus_makeEventBus();
  eventBus_on(eventBus, "SomeEvent", mock_fnListener1);
  eventBus_once(eventBus, "SomeEvent", mock_fnListener2);
  eventBus_off(eventBus, "SomeEvent", mock_fnListener2);
  Event *event = eventBus_getEvent(eventBus, "SomeEvent");
  mu_assert("error, eventBus->eventsLength != 1", eventBus->eventsLength == 1);
  mu_assert("error, event == NULL", event != NULL);
  mu_assert("error, event->name != SomeEvent", (strcmp(event->name, "SomeEvent") == 0));
  mu_assert("error, event->listenersLength != 0", event->listenersLength == 1);
  mu_assert("error, event->listeners != NULL", event->listeners != NULL);
  mu_assert("error, event->listeners[0].fn != mock_fnListener1", event->listeners[0].fn == mock_fnListener1);
  mu_assert("error, event->listeners[0].isOnce != false", event->listeners[0].isOnce == false);
  eventBus_freeEventBus(eventBus);
  return 0;
}

static char *test14_off_withTreeListenersRemoveTheMiddle() {
  EventBus *eventBus = eventBus_makeEventBus();
  eventBus_on(eventBus, "SomeEvent", mock_fnListener1);
  eventBus_once(eventBus, "SomeEvent", mock_fnListener2);
  eventBus_on(eventBus, "SomeEvent", mock_fnListener3);
  eventBus_off(eventBus, "SomeEvent", mock_fnListener2);
  Event *event = eventBus_getEvent(eventBus, "SomeEvent");
  mu_assert("error, eventBus->eventsLength != 1", eventBus->eventsLength == 1);
  mu_assert("error, event == NULL", event != NULL);
  mu_assert("error, event->name != SomeEvent", (strcmp(event->name, "SomeEvent") == 0));
  mu_assert("error, event->listenersLength != 0", event->listenersLength == 2);
  mu_assert("error, event->listeners != NULL", event->listeners != NULL);
  mu_assert("error, event->listeners[0].fn != mock_fnListener1", event->listeners[0].fn == mock_fnListener1);
  mu_assert("error, event->listeners[0].isOnce != false", event->listeners[0].isOnce == false);
  mu_assert("error, event->listeners[1].fn != mock_fnListener3", event->listeners[1].fn == mock_fnListener3);
  mu_assert("error, event->listeners[1].isOnce != false", event->listeners[1].isOnce == false);
  eventBus_freeEventBus(eventBus);
  return 0;
}

static char *test15_emit() {
  EventBus *eventBus = eventBus_makeEventBus();
  eventBus_on(eventBus, "SomeEvent", mock_fnListener1);
  Event *event = eventBus_getEvent(eventBus, "SomeEvent");
  mock_struct mockStruct;
  mockStruct.n = 9;
  eventBus_emit(eventBus, "SomeEvent", 1, &mockStruct);
  mu_assert("error, eventBus->eventsLength != 1", eventBus->eventsLength == 1);
  mu_assert("error, event == NULL", event != NULL);
  mu_assert("error, event->name != SomeEvent", (strcmp(event->name, "SomeEvent") == 0));
  mu_assert("error, event->listenersLength != 1", event->listenersLength == 1);
  mu_assert("error, event->listeners == NULL", event->listeners != NULL);
  mu_assert("error, event->listeners[0].fn != mock_fnListener1", event->listeners[0].fn == mock_fnListener1);
  mu_assert("error, event->listeners[0].isOnce != false", event->listeners[0].isOnce == false);
  mu_assert("error, mockStruct.n != 10", mockStruct.n == 10);
  eventBus_freeEventBus(eventBus);
  return 0;
}

static char *test16_emit_isOnceRemovedAfterCallFn() {
  EventBus *eventBus = eventBus_makeEventBus();
  eventBus_once(eventBus, "SomeEvent", mock_fnListener1);
  Event *event = eventBus_getEvent(eventBus, "SomeEvent");
  mock_struct mockStruct;
  mockStruct.n = 9;
  eventBus_emit(eventBus, "SomeEvent", 1, &mockStruct);
  mu_assert("error, eventBus->eventsLength != 1", eventBus->eventsLength == 1);
  mu_assert("error, event == NULL", event != NULL);
  mu_assert("error, event->name != SomeEvent", (strcmp(event->name, "SomeEvent") == 0));
  mu_assert("error, event->listenersLength != 1", event->listenersLength == 0);
  mu_assert("error, event->listeners != NULL", event->listeners == NULL);
  mu_assert("error, mockStruct.n != 10", mockStruct.n == 10);
  eventBus_freeEventBus(eventBus);
  return 0;
}

static char *test17_emit_withTwoEvents() {
  EventBus *eventBus = eventBus_makeEventBus();
  eventBus_on(eventBus, "SomeEvent", mock_fnListener1);
  eventBus_on(eventBus, "SomeEvent2", mock_fnListener2);  
  Event *event = eventBus_getEvent(eventBus, "SomeEvent");
  Event *event2 = eventBus_getEvent(eventBus, "SomeEvent2");
  mock_struct mockStruct;
  mockStruct.n = 9;
  eventBus_emit(eventBus, "SomeEvent", 1, &mockStruct);
  eventBus_emit(eventBus, "SomeEvent2", MockStruct, &mockStruct, End);
  mu_assert("error, eventBus->eventsLength != 2", eventBus->eventsLength == 2);
  mu_assert("error, event == NULL", event != NULL);
  mu_assert("error, event2 == NULL", event2 != NULL);
  mu_assert("error, event->name != SomeEvent", (strcmp(event->name, "SomeEvent") == 0));
  mu_assert("error, event2->name != SomeEvent2", (strcmp(event2->name, "SomeEvent2") == 0));
  mu_assert("error, event->listenersLength != 1", event->listenersLength == 1);
  mu_assert("error, event2->listenersLength != 1", event2->listenersLength == 1);
  mu_assert("error, event->listeners == NULL", event->listeners != NULL);
  mu_assert("error, event2->listeners == NULL", event2->listeners != NULL);
  mu_assert("error, event->listeners[0].fn != mock_fnListener1", event->listeners[0].fn == mock_fnListener1);
  mu_assert("error, event->listeners[0].isOnce != false", event->listeners[0].isOnce == false);
  mu_assert("error, event2->listeners[0].fn != mock_fnListener2", event2->listeners[0].fn == mock_fnListener2);
  mu_assert("error, event2->listeners[0].isOnce != false", event2->listeners[0].isOnce == false);
  mu_assert("error, mockStruct.n != 10", mockStruct.n == 11);
  eventBus_freeEventBus(eventBus);
  return 0;
}

static char *test18_getListenerCountOfEvent() {
  EventBus *eventBus = eventBus_makeEventBus();
  eventBus_once(eventBus, "SomeEvent", mock_fnListener1);
  eventBus_once(eventBus, "SomeEvent", mock_fnListener2);
  eventBus_on(eventBus, "SomeEvent", mock_fnListener3);
  eventBus_on(eventBus, "SomeEvent", mock_fnListener1);
  eventBus_on(eventBus, "SomeEvent2", mock_fnListener1);
  Event *event = eventBus_getEvent(eventBus, "SomeEvent");
  Event *event2 = eventBus_getEvent(eventBus, "SomeEvent2");
  int listenerCountEvent = eventBus_getListenerCountOfEvent(eventBus, "SomeEvent");
  int listenerCountEvent2 = eventBus_getListenerCountOfEvent(eventBus, "SomeEvent2");
  mu_assert("error, listenerCountEvent != 4", listenerCountEvent == 4);
  mu_assert("error, listenerCountEvent2 != 1", listenerCountEvent2 == 1);
  mu_assert("error, eventBus->eventsLength != 2", eventBus->eventsLength == 2);
  mu_assert("error, event == NULL", event != NULL);
  mu_assert("error, event->name != SomeEvent", (strcmp(event->name, "SomeEvent") == 0));
  mu_assert("error, event->listenersLength != 4", event->listenersLength == 4);
  mu_assert("error, event->listeners == NULL", event->listeners != NULL);
  mu_assert("error, event2 == NULL", event != NULL);
  mu_assert("error, event2->name != SomeEvent", (strcmp(event2->name, "SomeEvent2") == 0));
  mu_assert("error, event2->listenersLength != 1", event2->listenersLength == 1);
  mu_assert("error, event2->listeners == NULL", event2->listeners != NULL);
  eventBus_freeEventBus(eventBus);
  return 0;
}

static char *test19_getAllListenersOfEvent() {
  EventBus *eventBus = eventBus_makeEventBus();
  eventBus_once(eventBus, "SomeEvent", mock_fnListener1);
  eventBus_once(eventBus, "SomeEvent", mock_fnListener2);
  eventBus_on(eventBus, "SomeEvent", mock_fnListener3);
  Event *event = eventBus_getEvent(eventBus, "SomeEvent");
  Listener *listeners = malloc(event->listenersLength * sizeof(Listener));
  eventBus_getAllListenersOfEvent(eventBus, "SomeEvent", listeners);
  mu_assert("error, eventBus->eventsLength != 1", eventBus->eventsLength == 1);
  mu_assert("error, event == NULL", event != NULL);
  mu_assert("error, event->name != SomeEvent", (strcmp(event->name, "SomeEvent") == 0));
  mu_assert("error, event->listenersLength != 3", event->listenersLength == 3);
  mu_assert("error, event->listeners == NULL", event->listeners != NULL);
  mu_assert("error, listeners[0].fn != mock_fnListener1", listeners[0].fn == mock_fnListener1);
  mu_assert("error, listeners[0].isOnce != true", listeners[0].isOnce == true);
  mu_assert("error, listeners[1].fn != mock_fnListener2", listeners[1].fn == mock_fnListener2);
  mu_assert("error, listeners[1].isOnce != true", listeners[1].isOnce == true);
  mu_assert("error, listeners[2].fn != mock_fnListener3", listeners[2].fn == mock_fnListener3);
  mu_assert("error, listeners[2].isOnce != false", listeners[2].isOnce == false);
  eventBus_freeEventBus(eventBus);
  free(listeners);
  return 0;
}

static char *test20_getAllEventNames() {
  EventBus *eventBus = eventBus_makeEventBus();
  eventBus_once(eventBus, "SomeEvent", mock_fnListener1);
  eventBus_once(eventBus, "SomeEvent2", mock_fnListener2);
  eventBus_on(eventBus, "SomeEvent3", mock_fnListener3);
  const char **eventNames = malloc(3 * sizeof(char*));
  eventBus_getAllEventNames(eventBus, eventNames);
  mu_assert("error, eventBus->eventsLength != 3", eventBus->eventsLength == 3);
  mu_assert("error, eventNames[0] == SomeEvent", (strcmp(eventNames[0], "SomeEvent") == 0));
  mu_assert("error, eventNames[1] == SomeEvent2", (strcmp(eventNames[1], "SomeEvent2") == 0));
  mu_assert("error, eventNames[2] == SomeEvent3", (strcmp(eventNames[2], "SomeEvent3") == 0));
  eventBus_freeEventBus(eventBus);
  return 0;
}

static char *test21_removeAllListenersOfEvent() {
  EventBus *eventBus = eventBus_makeEventBus();
  eventBus_once(eventBus, "SomeEvent", mock_fnListener1);
  eventBus_once(eventBus, "SomeEvent", mock_fnListener2);
  eventBus_on(eventBus, "SomeEvent", mock_fnListener3);
  Event *event = eventBus_getEvent(eventBus, "SomeEvent");
  eventBus_removeAllListenersOfEvent(eventBus, "SomeEvent");
  mu_assert("error, eventBus->eventsLength != 1", eventBus->eventsLength == 1);
  mu_assert("error, event == NULL", event != NULL);
  mu_assert("error, event->name != SomeEvent", (strcmp(event->name, "SomeEvent") == 0));
  mu_assert("error, event->listenersLength != 0", event->listenersLength == 0);
  mu_assert("error, event->listeners != NULL", event->listeners == NULL);
  eventBus_freeEventBus(eventBus);
  return 0;
}

static char *test22_removeAllListenersOfEvent_withAddListenerEvent() {
  EventBus *eventBus = eventBus_makeEventBus();
  eventBus_once(eventBus, "addListener", mock_fnListener1);
  eventBus_on(eventBus, "addListener", mock_fnListener2);
  Event *event = eventBus_getEvent(eventBus, "addListener");
  eventBus_removeAllListenersOfEvent(eventBus, "addListener");
  mu_assert("error, eventBus->eventsLength != 1", eventBus->eventsLength == 1);
  mu_assert("error, event == NULL", event != NULL);
  mu_assert("error, event->name != addListener", (strcmp(event->name, "addListener") == 0));
  mu_assert("error, event->listenersLength != 0", event->listenersLength == 0);
  mu_assert("error, event->listeners != NULL", event->listeners == NULL);
  eventBus_freeEventBus(eventBus);
  return 0;
}

static char *test23_removeAllListenersOfEvent_withRemoveListenerEvent() {
  EventBus *eventBus = eventBus_makeEventBus();
  eventBus_once(eventBus, "removeListener", mock_fnListener4);
  eventBus_on(eventBus, "removeListener", mock_fnListener4);
  Event *event = eventBus_getEvent(eventBus, "removeListener");
  eventBus_removeAllListenersOfEvent(eventBus, "removeListener");
  mu_assert("error, eventBus->eventsLength != 1", eventBus->eventsLength == 1);
  mu_assert("error, event == NULL", event != NULL);
  mu_assert("error, event->name != removeListener", (strcmp(event->name, "removeListener") == 0));
  mu_assert("error, event->listenersLength != 0", event->listenersLength == 0);
  mu_assert("error, event->listeners != NULL", event->listeners == NULL);
  eventBus_freeEventBus(eventBus);
  return 0;
}


static char *test24_addListenerEvent() {
  mockCount = 0;
  EventBus *eventBus = eventBus_makeEventBus();
  eventBus_on(eventBus, "SomeEvent", mock_fnListener1);
  eventBus_on(eventBus, "SomeEvent2", mock_fnListener2);
  mock_struct mockStruct;
  mockStruct.n = 9;
  eventBus_on(eventBus, "addListener", mock_fnListener4);
  eventBus_once(eventBus, "addListener", mock_fnListener4);
  eventBus_emit(eventBus, "SomeEvent", 1, &mockStruct);
  eventBus_emit(eventBus, "SomeEvent2", MockStruct, &mockStruct, End);
  mu_assert("error, eventBus->eventsLength != 3", eventBus->eventsLength == 3);
  mu_assert("error, mockCount != 1", mockCount == 1);
  mu_assert("error, mockStruct.n != 11", mockStruct.n == 11);
  eventBus_freeEventBus(eventBus);
  return 0;
}

static char *test25_removeListenerEvent() {
  mockCount = 0;
  EventBus *eventBus = eventBus_makeEventBus();
  eventBus_on(eventBus, "SomeEvent", mock_fnListener1);
  eventBus_on(eventBus, "SomeEvent2", mock_fnListener2);
  mock_struct mockStruct;
  mockStruct.n = 9;
  eventBus_on(eventBus, "removeListener", mock_fnListener4);
  eventBus_once(eventBus, "removeListener", mock_fnListener4);
  eventBus_emit(eventBus, "SomeEvent", 1, &mockStruct);
  eventBus_emit(eventBus, "SomeEvent", 1, &mockStruct);
  eventBus_emit(eventBus, "SomeEvent2", MockStruct, &mockStruct, End);
  eventBus_removeAllListenersOfEvent(eventBus, "SomeEvent");
  mu_assert("error, eventBus->eventsLength != 3", eventBus->eventsLength == 3);
  mu_assert("error, mockCount != 3", mockCount == 3);
  mu_assert("error, mockStruct.n != 12", mockStruct.n == 12);
  eventBus_freeEventBus(eventBus);
  return 0;
}


static char *ALL_TESTS() {
  mu_run_test(test1_addEvent);
  mu_run_test(test2_getEvent);
  mu_run_test(test3_addListenerInEvent);
  mu_run_test(test4_on);
  mu_run_test(test5_on_twoCalls);
  mu_run_test(test6_on_whenFnIsNULL);
  mu_run_test(test7_once);
  mu_run_test(test8_once_whenFnIsNULL);
  mu_run_test(test9_getListenerIndexByFn);
  mu_run_test(test10_getListenerIndexByFn_whenListenerNotExist);
  mu_run_test(test11_off);
  mu_run_test(test12_off_withTwoListenersRemoveTheFirst);
  mu_run_test(test13_off_withTwoListenersRemoveTheSecond);
  mu_run_test(test14_off_withTreeListenersRemoveTheMiddle);
  mu_run_test(test15_emit);
  mu_run_test(test16_emit_isOnceRemovedAfterCallFn);
  mu_run_test(test17_emit_withTwoEvents);
  mu_run_test(test18_getListenerCountOfEvent);
  mu_run_test(test19_getAllListenersOfEvent);
  mu_run_test(test20_getAllEventNames);
  mu_run_test(test21_removeAllListenersOfEvent);
  mu_run_test(test22_removeAllListenersOfEvent_withAddListenerEvent);
  mu_run_test(test23_removeAllListenersOfEvent_withRemoveListenerEvent);
  mu_run_test(test24_addListenerEvent);
  mu_run_test(test25_removeListenerEvent);
  return 0;
}

int main(int argc, char **argv) {
  char *result = ALL_TESTS();
  if(result != 0)
    printf("%s\n", result);
  else
    printf("ALL TESTS PASSED\n");
  printf("Tests run: %d\n", tests_run);

  return result != 0;
}
