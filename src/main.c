#include <stdio.h>
#include "eventbus.h"

typedef struct SomeStruct1 {
  int a;
} SomeStruct1;

typedef struct SomeStruct2 {
  int b;
} SomeStruct2;

typedef enum myTypes {
  Some_Struct1, Some_Struct2,
  myEnd
} myTypes;

void fnListener1(int type, va_list args);
void fnListener2(int type, va_list args);
void fnListener3(int type, va_list args);
void fnListener4(int type, va_list args);

int main() {
  EventBus *eventBus = eventBus_makeEventBus();

  eventBus_on(eventBus, "onEvent", fnListener1);
  eventBus_once(eventBus, "onceEvent", fnListener2);

  // The third argument is the number of argument that is passsed
  // The fourth onwards is the arguments passed
  eventBus_emit(eventBus, "onEvent", 1, 1);
  eventBus_emit(eventBus, "onEvent", 1, 1);

  eventBus_emit(eventBus, "onceEvent", 1, 2);
  eventBus_emit(eventBus, "onceEvent", 1, 2);

  /* Print:
     fnListener1: 1
     fnListener1: 1
     fnListener2: 2
  */

  // You also can use enums to pass the arguments, see:
  // Default types: String, Integer, Double, Float = Double
  eventBus_on(eventBus, "onEventDefaultTypes", fnListener3);
  eventBus_emit(eventBus, "onEventDefaultTypes", String, "hi", End);

  /* Print:
     char arg[0]: hi
  */

  // You also can use your own enums, see:
  eventBus_on(eventBus, "onEventOwnTypes", fnListener4);
  eventBus_emit(eventBus, "onEventOwnTypes", Some_Struct1, (SomeStruct1){1}, Some_Struct2, (SomeStruct2){2}, myEnd);

  /* Print:
     fnListener4: 1
     fnListener4: 2
  */

  eventBus_freeEventBus(eventBus);
  return 0;
}

// --------------------------
// To emit normal mode
// --------------------------
void fnListener1(int type, va_list args) {
  int count = 0;
  while (count < type) {
    int n = va_arg(args, int);
    printf("fnListener1: %d\n", n);
    count++;
  }
}

void fnListener2(int type, va_list args) {
  int count = 0;
  while (count < type) {
    int n = va_arg(args, int);
    printf("fnListener2: %d\n", n);
    count++;
  }
}

// --------------------------
// To emit type mode
// --------------------------
void fnListener3(int type, va_list args) {
  int count;
  count = 0;
  while (type != End) {
    switch (type) {
    case String:
      fprintf(stdout, "char arg[%d]: %s\n", count, va_arg(args, const char *));
      break;
    case Integer:
      fprintf(stdout, "int arg[%d]: %d\n", count, va_arg(args, int));
      break;
    case Double:
      fprintf(stdout, "double arg[%d]: %f\n", count, va_arg(args, double));
      break;
    default:
      fprintf(stderr, "unknown type specifier\n");
      break;
    }
    type = va_arg(args, enum types);
    count++;
  }
}

void fnListener4(int type, va_list args) {
  int count;
  count = 0;
  while (type != myEnd) {
    switch (type) {
    case Some_Struct1:
      printf("fnListener4: %d\n", va_arg(args, SomeStruct1).a);
      break;
    case Some_Struct2:
      printf("fnListener4: %d\n", va_arg(args, SomeStruct2).b);
      break;
    default:
      fprintf(stderr, "unknown type specifier\n");
      break;
    }
    type = va_arg(args, enum myTypes);
    count++;
  }
}
