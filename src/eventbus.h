#ifndef _EVENT_BUS_H_
#define _EVENT_BUS_H_

#include <stdarg.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>

typedef enum types {
  String, Integer, Double, Float = Double,
  End
} types;

typedef struct Listener {
  void (*fn)(int type, va_list args);
  bool isOnce;
} Listener;

typedef struct Event {
  const char *name;
  Listener *listeners;
  int listenersLength;
} Event;

typedef struct EventBus {
  Event *events;
  int eventsLength;
} EventBus;

EventBus *eventBus_makeEventBus(void);
void eventBus_freeEventBus(EventBus *eventBus);
EventBus *eventBus_on(EventBus *eventBus, const char *eventName, void (*fn)(int type, va_list args));
EventBus *eventBus_once(EventBus *eventBus, const char *eventName, void (*fn)(int type, va_list args));
EventBus *eventBus_off(EventBus *eventBus, const char *eventName, void (*fn)(int type, va_list args));
// TODO - If need to better performance, so instead realloc, disable the event:
// EventBus *disable(EventBus *eventBus, const char *eventName, void (*fn)(int type, va_list args));
EventBus *eventBus_emit(EventBus *eventBus, const char *eventName, int type, ...);
int eventBus_getListenerCountOfEvent(EventBus *eventBus, const char *eventName);
void eventBus_getAllListenersOfEvent(EventBus *eventBus, const char *eventName, Listener *listeners);
void eventBus_getAllEventNames(EventBus *eventBus, const char *eventNames[]);
EventBus *eventBus_removeAllListenersOfEvent(EventBus *eventBus, const char *eventName);

// ----- Internal ------------------------------------------------
// It is not safe to manipulate pointers manually
// without using the external API, dynamic arrays
// are modified and you may end up losing the pointer reference.
// ---------------------------------------------------------------
static Event *eventBus_addEvent(EventBus *eventBus, const char *eventName);
static Event *eventBus_getEvent(EventBus *eventBus, const char *eventName);
static int eventBus_addListenerInEvent(Event *event, void (*fn)(int type, va_list args), bool isOnce);
static int eventBus_getListenerIndexByFn(Event *event, void (*fn)(int type, va_list args));

EventBus *eventBus_makeEventBus(void) {
  EventBus *eventBus;
  struct Event *events;
  events = NULL;
  eventBus = (EventBus*) malloc(sizeof(EventBus) * 1);
  *eventBus = (EventBus){events, 0};
  return eventBus;
}

void eventBus_freeEventBus(EventBus *eventBus) {
  for(int i = 0; i < eventBus->eventsLength; i++)
    free(eventBus->events[i].listeners);
  free(eventBus->events);
  free(eventBus);
}

EventBus *eventBus_on(EventBus *eventBus, const char *eventName, void (*fn)(int type, va_list args)) {
  Event *addListenerEvent = eventBus_getEvent(eventBus, "addListener");
  if(addListenerEvent) eventBus_emit(eventBus, "addListener", 0);
  Event *event = eventBus_getEvent(eventBus, eventName);
  if(!event) event = eventBus_addEvent(eventBus, eventName);
  if(eventBus_addListenerInEvent(event, fn, false) != 0)
    fprintf(stderr, "EventBus on, Warning: The listener didn't added in the \"%s\" event because fn is NULL.\n", eventName);
  return eventBus;
}

EventBus *eventBus_once(EventBus *eventBus, const char *eventName, void (*fn)(int type, va_list args)) {
  Event *addListenerEvent = eventBus_getEvent(eventBus, "addListener");
  if(addListenerEvent) eventBus_emit(eventBus, "addListener", 0);
  Event *event = eventBus_getEvent(eventBus, eventName);
  if(!event) event = eventBus_addEvent(eventBus, eventName);
  if(eventBus_addListenerInEvent(event, fn, true) != 0)
    fprintf(stderr, "EventBus once, Warning: The listener didn't added in the \"%s\" event because fn is NULL.\n", eventName);
  return eventBus;
}

EventBus *eventBus_off(EventBus *eventBus, const char *eventName, void (*fn)(int type, va_list args)) {
  Event *event = eventBus_getEvent(eventBus, eventName);
  if(!event) fprintf(stderr, "EventBus off, Warning: Event \"%s\" doesn't exist.\n", eventName);
  int listenersLength = event->listenersLength;
  Listener *newListeners = malloc((listenersLength - 1) * sizeof(Listener));
  int listenerIndexToRemove = eventBus_getListenerIndexByFn(event, fn);
  if(listenerIndexToRemove >= 0) {
    memcpy(newListeners, event->listeners, listenerIndexToRemove * sizeof(Listener));
    if(listenerIndexToRemove != (listenersLength - 1))
      memcpy(newListeners + listenerIndexToRemove,
	     event->listeners + listenerIndexToRemove + 1,
	     (listenersLength - listenerIndexToRemove - 1) * sizeof(Listener));
    free(event->listeners);
    event->listeners = newListeners;
    event->listenersLength--;
    Event *removeListenerEvent = eventBus_getEvent(eventBus, "removeListener");
    if(removeListenerEvent) eventBus_emit(eventBus, "removeListener", 0);
    if(event->listenersLength <= 0) event->listeners = NULL;
  }
  return eventBus;
}

EventBus *eventBus_emit(EventBus *eventBus, const char *eventName, int type, ...) {
  va_list args;
  va_start(args, type);
  Event *event = eventBus_getEvent(eventBus, eventName);
  if(event) {
    if(event->listeners) {
      Listener *listener = event->listeners;
      void (*fn)(int type, va_list args);
      for(int i = 0; i < event->listenersLength; i++) {
	fn = listener[i].fn;
	if(listener[i].isOnce) eventBus_off(eventBus, eventName, listener[i].fn);
	fn(type, args);
	//printf("emit [%d]: %s\n", i, event->name);
      }
    }
  }
  va_end(args);
  return eventBus;
}

int eventBus_getListenerCountOfEvent(EventBus *eventBus, const char *eventName) {
  Event *event = eventBus_getEvent(eventBus, eventName);
  if(!event) fprintf(stderr, "EventBus getListenerCountOfEvent, Warning: Event \"%s\" doesn't exist.\n", eventName);
  return event->listenersLength;
}

void eventBus_getAllListenersOfEvent(EventBus *eventBus, const char *eventName, Listener *listeners) {
  Event *event = eventBus_getEvent(eventBus, eventName);
  if(!event) fprintf(stderr, "EventBus getAllListenersOfEvent, Warning: Event \"%s\" doesn't exist.\n", eventName);
  if(event->listeners)
    memcpy(listeners, event->listeners, event->listenersLength * sizeof(Listener));
}

void eventBus_getAllEventNames(EventBus *eventBus, const char *eventNames[]) {
  Event *event;
  for(int i = 0; i < eventBus->eventsLength; i++) {
    event = &eventBus->events[i];
    if(event) eventNames[i] = (char*)event->name;
  }
}

EventBus *eventBus_removeAllListenersOfEvent(EventBus *eventBus, const char *eventName) {
  Event *event = eventBus_getEvent(eventBus, eventName);
  if(!event) fprintf(stderr, "EventBus removeAllListenersOfEvent, Warning: Event \"%s\" doesn't exist.\n", eventName);
  Listener *listener = &event->listeners[0];
  if(listener) {
    eventBus_off(eventBus, eventName, listener->fn);
    return eventBus_removeAllListenersOfEvent(eventBus, eventName);
  }
  return eventBus;
}

// ----- Internal ------------------------------------------------
// It is not safe to manipulate pointers manually
// without using the external API, dynamic arrays
// are modified and you may end up losing the pointer reference.
// ---------------------------------------------------------------
static Event *eventBus_addEvent(EventBus *eventBus, const char *eventName) {
  struct Event *event;
  eventBus->events = (Event*) realloc(eventBus->events, sizeof(Event) * (eventBus->eventsLength + 1));
  event = &eventBus->events[eventBus->eventsLength];
  eventBus->eventsLength++;
  event->listeners = NULL;
  event->name = eventName;
  event->listenersLength = 0;
  return event;
}

static Event *eventBus_getEvent(EventBus *eventBus, const char *eventName) {
  Event *event;
  for(int i = 0; i < eventBus->eventsLength; i++) {
    event = &eventBus->events[i];
    if(event) {
      //printf("getEvent: %s\n", event->name);
      if(strcmp(event->name, eventName) == 0)
	return event;
    }
  }
  return NULL;
}

static int eventBus_addListenerInEvent(Event *event, void (*fn)(int type, va_list args), bool isOnce) {
  if(fn) {
    event->listeners = (Listener*) realloc(event->listeners, sizeof(Listener) * (event->listenersLength + 1));
    Listener *listener = &event->listeners[event->listenersLength];
    event->listenersLength++;
    listener->fn = fn;
    listener->isOnce = isOnce;
    //printf("addListenerInEvent: %s\n", event->name);
    return 0;
  }
  return 1;
}

static int eventBus_getListenerIndexByFn(Event *event, void (*fn)(int type, va_list args)) {
  if(event)
    if(event->listeners) {
      Listener *listener = event->listeners;
      for(int i = 0; i < event->listenersLength; i++)
	if(listener[i].fn == fn) return i;
    }
  return -1;
}

#endif // #ifndef _EVENT_BUS_H_
